# Phenols descriptives analysis drake plan
# M. Rolland
# 17/06/19

phenols_descriptive <- drake_plan(
  
  # Data import ----
  limits = read_csv(file_in("data/limits_02_09-19.csv")),
  crb = read_csv("data/crb_11-10-19.csv"),
  samples = read_csv(file_in("data/samples_02_09_19.csv")),
  bdd_grossesse = read_stata(file_in("data/bdd_grossesse_v3.dta")),
  post_nat = read_stata(file_in("data/demande_181205.dta")),
  jobs_to_jobs_cat = read.xlsx(file_in("data/jobs_to_jobs_cat_3.xlsx"), 1),
  t3_new_jobs = read_csv(file_in("data/t3_new_jobs.csv")),
  tobacco = read_stata(file_in("data/Tobacco_cor.dta")),
  fig_1_data = read_csv(file_in("data/fig_1_data.csv")),

  # Data management ----
  # * CRB data ----
  crb_data = crb %>%
    select(INSERM_ID, starts_with("sample"), contains("date"), contains("time"), 
           contains("gravity"), freezer_type2, n_sample),
  
  # * Samples data ----
  samples_data = samples %>%
    select(-contains("_string"), -contains("dup"), -contains("NIPH"),
           -c("MEP", "MiBP", "MnBP", "MBzP", "MEHP", "MEHHP", "MEOHP", 
              "MECPP", "MMCHP", "ohMiNP", "oxoMiNP", "cxMiNP", "sek_phtal",
              "ohMINCH", "oxoMINCH", "ohMPHP", "DiNP_ms", "DINCH_ms", 
              "DEHP_ms", "code_sepages", "_merge")) %>%
    mutate(ident = as.character(ident)),
  
  # * Questionnaires data ---- 
  # rename vars, create simple vars, select vars used in analysis
  quest_data = prepare_quest_data(bdd_grossesse, post_nat, crb, tobacco),
  
  # * Mom occupation ----
  occupation_data = prepare_mom_occupation(bdd_grossesse, jobs_to_jobs_cat, 
                                           t3_new_jobs, crb),
  # * Milk data ---- 
  # (breastfeeding, formula, etc)
  milk_data = prepare_milk_data(post_nat, crb),

  # * Phenols data ----
  # used in all further analyses, with one line per ident and per period
  phenols_data = left_join(crb_data, samples_data, by = "INSERM_ID") %>%
    left_join(quest_data, by = c("ident", "period")) %>%
    left_join(occupation_data, by = c("ident", "period")) %>%
    left_join(milk_data, by = c("ident", "period")) %>%
    # prepare variables: factor orders, type
    prepare_vars(.),
  # 
  # * Phenols long format data ----
  # transform to long format, compute sg adjusted values, add LOD, LOQ
  # used for descriptive analyses
  phenols_data_long = make_long_data(phenols_data, limits),

  # * Box cox transform + interval endpoints ----
  bc_data = create_bc_data(phenols_data_long),

  # * Ratio data ----
  # with one line per phenol and one col for free and one col for total
  ratio_data = create_ratio_data(samples_data, limits),

  # Graphical abstract ----
  # * SG corrected boxplots ----
  phenol_boxplots = plot_boxplots(phenols_data_long),

  # Descriptive analysis ----
  # * Table 1: individual level descriptive table ----
  # population variables that do not change over time such as BMI at inclusion
  population_description_tab = describe_population(phenols_data),

  # * Table 2: sample level descriptive table ----
  # population variables that can evolve over time, such as occupation
  sample_level_description_tab = describe_samples(phenols_data),

  # * Table 3: raw and SG adsjusted phenol level descriptive table ----
  # raw levels
  total_levels_tab = describe_levels(phenols_data_long,
                                     descriptive_compounds(),
                                     "val"),
  # SG adjusted levels
  total_sg_levels_tab = describe_levels(phenols_data_long,
                                        descriptive_compounds(),
                                        "val_sg"),

  # * Table 4: correlation between periods ----
  period_correlation = correlate_periods(phenols_data_long),

  # * Table S1: sampling material levels ----
  # by hand from SEPAGES_sampling_material_results

  # * Table S2: ratios table ----
  ratios_tab = compute_ratios(ratio_data),

  # * Table S3: bootstrapped regression between free and conjugated ----
  # with n_boot = number of bootstrap iterations
  boot_results = free_conj_regression(ratio_data, n_boot = 1000),

  # * Table S4: free phenol level descriptive table ----
  free_levels_tab = describe_levels(phenols_data_long,
                                    free_compounds(),
                                    "val"),

  # * Table S5: correlation between compounds during pregnancy ----
  mother_correlations = correlate_compounds(phenols_data, c("T1", "T3")),

  # * Table S6: correlation between compounds infants ----
  infant_correlations = correlate_compounds(phenols_data, c("M2", "Y1")),

  # * Figure 2: free ~ conj regression plot ----
  #free_conj_plot = plot_free_conj2(samples_data),

  # Stat models ----
  # * Data imputation ----
  #Impute model data separately for pregnancy and post nat data The reason for
  #doing it separately is that I don't know (at the moment) how to modify
  #imputed data prior to applying the model
  mother_data_imputed = phenols_data %>%
    filter(period %in% c("T1", "T3")) %>%
    select(mother_model_vars()) %>% # select preg model variables
    left_join(bc_data, by = c("ident", "period")) %>%
    impute_data(.),

  infant_data_imputed = phenols_data %>%
    filter(period %in% c("M2", "Y1")) %>%
    select(infant_model_vars()) %>%
    left_join(bc_data, by = c("ident", "period")) %>%
    impute_data(.),

  # * Table 5: Mother models ----
  mother_models = fit_imputed(mother_data_imputed, "mother", mother_compounds()),

  # * Table 6: Infant models ----
  infant_models = fit_imputed(infant_data_imputed, "infant", infant_compounds()),

  # Discussion ----
  # * Figure 1: cohort level comparison ----
  #cohort_hist = plot_cohort_hist(fig_1_data),

  # Sensitivity analyses ----
  # * Table S7: Mother complete models ----
  mother_models_complete = fit_complete(phenols_data_long, "mother", mother_compounds()),

  # * Table S8: Infant complete models ----
  infant_models_complete = fit_complete(phenols_data_long, "infant", infant_compounds()),

  # * Complete model Ns ----
  complete_model_n = get_complete_n(phenols_data_long),

  # * Table S9: Conjugated compound models ----
  conj_models = fit_imputed(infant_data_imputed, "infant", conj_compounds()),

  # * Table S10: Mother logistic regression for BPS and BUPA ----
  mother_models_logistic = fit_logistic(mother_data_imputed, c("BPS", "BUPA"), "mother"),

  # * Table S11: Infant logistic regression for BPS and BUPA ----
  infant_models_logistic = fit_logistic(infant_data_imputed, c("BPS", "BUPA"), "infant"),

  # * Table S12: Mother models without smoke, canned food and occupation ----
  non_sparse_models = fit_non_sparse(mother_data_imputed),

  # Table S13: BPA - BPS models limited to the second trimester ----
  t2_models = fit_t2(phenols_data, bc_data),

  # Documents ----
  # * Variable coding ----

  # * Data imputation ----

  # * Main results ----
  main_results = rmarkdown::render(
    knitr_in("results/main_results.Rmd"),
    output_file = file_out(!!str_c("main_results_", Sys.Date(), ".html")),
    output_dir = "results",
    quiet = TRUE
  ),

  # * Model residuals ----

  # * Sensitivity analysis ----
  sensitivity_analysis = rmarkdown::render(
    knitr_in("results/sensitivity_analyses.Rmd"),
    output_file = file_out(!!str_c("sensitivity_analyses_", Sys.Date(), ".html")),
    output_dir = "results",
    quiet = TRUE
  ),

  # * Supplementary material ----
  supplementary_material = rmarkdown::render(
    knitr_in("results/supplementary_material.Rmd"),
    output_file = file_out(!!str_c("supplementary_material_", Sys.Date(), ".html")),
    output_dir = "results",
    quiet = TRUE
  ),

  # * Export session info ----
  session_info = writeLines(capture.output(sessionInfo()), "sessionInfo.txt")
)

